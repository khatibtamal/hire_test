pragma solidity ^0.4.1;

contract RandomTest {
    
    /*
    total ether required in wei, to play the game
    */
    uint public constant REQ_AMNT_WEI = 20000000;
    
    /*
    cost to transfer funds in wei
    */
    uint public constant FUND_TRANSFER_COST_WEI = 21000;
    
    /*
    total players required that will be in game
    */
    uint public constant TTL_REQ_PLRS = 3;
    
    /*
    total number of blocks to wait
    */
    uint public constant BLKS_TO_WAIT = 50;
    
    /*
    Log the creator of the smart contract
    */
    address public creator;
    
    /*
    mapping of all players to an int greater than 0
    motivation is to conviniently determine if a player is
    already in the game or not
    */
    mapping(address => uint) public plAddresesToInt;
    
    /*
    mapping of corresponding int to player address
    motivation is to conviniently determine the winners
    address
    */
    mapping(uint => address) public intToPlAddress;
    
    /*
    total players in the game
    */
    uint public totalPlayers = 0;
    
    /*
    just a salt
    */
    uint public salt = 123;

    /*
    constructor
    */
    constructor() public {
        creator = msg.sender;
    }
    
    /*
    function to join game 
    requires predefined wei amount exactly and the player is not in game yet
    */
    function joinGame() public payable {
        require(msg.value == REQ_AMNT_WEI && !isPlayerAlreadyInGame() && totalPlayers < TTL_REQ_PLRS);
        
        intToPlAddress[totalPlayers++] = msg.sender;
        plAddresesToInt[msg.sender] = totalPlayers;
        
        if(totalPlayers == TTL_REQ_PLRS) {
            finishGame();
        }
    }
    
    /*
    verifies if a player is already in the game
    */
    function isPlayerAlreadyInGame() private view returns (bool) {
        return plAddresesToInt[msg.sender] > 0;
    }
    
    /*
    function to get random number to determine winner
    the number generated is between 0 to totalPlayers - 1
    */
    function getRandomNumber() private returns (uint) {
        return uint(keccak256(uint(block.blockhash(block.number - 1)) + salt)) % totalPlayers;
    }
    
    /*
    sends balance wei in the account to the specified address
    */
    function sendWeiToWinner(address winner) private {
        winner.send(address(this).balance - FUND_TRANSFER_COST_WEI);
    }
    
        /*
    finish off the game by randomly selecting a winner
    */
    function finishGame() private {
        sendWeiToWinner(intToPlAddress[getRandomNumber()]);
    }
}
